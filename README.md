# Development Environment

Mock services for development environment

To start all services run:

```sh
docker-compose up --build
```

## Dependencies

You must have `docker` and `docker-compose` installed

## Documentum

To mock documentum, an nginx server is started with basic auth and
serving as a proxy for [lorempixel](http://lorempixel.com)

Basic auth is setup with user and password: `user:test`

Example to download an image:

```sh
curl -o image.jpg -u "user:test" localhost:80/200/300/sports/1/
```

For reach like examples in the db, you can do the following.

```sh
curl -o image.jpg -u "user:test" http://prod.reach.dctm.cloud.walmart.com/dctm-rest/repositories/REACH_PROD/objects/271a177b-7ee0-4ef3-9fe3-5d5a88d7db39/content-media
```

In order to reach that url from the localhost ( not the container ) you need to modify your hots file like follows:

```
# hosts
...
127.0.0.1 prod.reach.dctm.cloud.walmart.com
...
```

## Azure blob storage

To mock Azure blob storage, we are using [Azurite](https://github.com/Azure/Azurite)

This emulates an azure blob storage server in port `10000`
It uses the following information:

```
Account name: devstoreaccount1
Account key: Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==
```

To create the container for testing run the following:

```sh
AZURE_STORAGE_CONNECTION_STRING="UseDevelopmentStorage=true" az storage container create --name <container_name> --public-access container
```

As an alternative, you can create containers using the azure SDK.

other connection strings settings that works are:

```
AZURE_CONNECTION_STRING=DefaultEndpointsProtocol=http;AccountName=devstoreaccount1;AccountKey=Eby8vdM02xNOcqFlqUwJPLlmEtlCDXJ1OUzFT50uSRZ6IFsuFq2UVErCz4I6tq/K1SZFPTOtr/KBHBeksoGMGw==;BlobEndpoint=http://azure:10000/devstoreaccount1;QueueEndpoint=http://azure:10001/devstoreaccount1;
```

Example request:

```sh
curl -o <file> http://localhost:10000/devstoreaccount1/<container>/<file>
```

Note that SDK already provide tools to get the url of a file, try to use that when uploading a file to azure.

Javascript example:

```javascript

const blobName = `file-${file.id}`;
containerClient.getBlockBlobClient(blobName);
const blockBlobClient = containerClient.getBlockBlobClient(blobName);
await blockBlobClient.upload(
  file.buffer.toString(),
  file.buffer.toString().length,
)
console.log(blockBlobClient.url}); // http://localhost:10000/devstoreaccount1/cont/file-3
```

## MySQL database

The mysql database has been mocked with two tables ( it contains actually 3, migrations table was used to create the mock db)

It contains:

**responses**

| column     |    type     | default | example                                                                                                                                             |
| ---------- | :---------: | ------: | --------------------------------------------------------------------------------------------------------------------------------------------------- |
| responseid | varchar(36) |         | 00044586-54a9-4fd3-b72b-7a6ee1b306f0                                                                                                                |
| value      | mediumblob  |         | ["http://prod\\.reach\\.dctm\\.cloud\\.walmart\\.com/dctm-rest/repositories/REACH_PROD/objects/ea00fd34-64b6-46fb-81cb-a6765f3adce4/content-media"] |

**migrated-responses**

| column     |    type     |   default   |
| ---------- | :---------: | :---------: |
| id         |  int auto   |             |
| responseid | varchar(36) |             |
| value      | mediumblob  |             |
| newValue   | mediumblob  |             |
| status     | varchar(36) | not-started |

The tables has been created following the production schema for reach including types.

The dump already contains data mocked in responses table that can be used for poc/test purposes.

Database information:

```
user: user
password: password
db: poc
port: 3306
host: localhost ( or linked name if you are using docker-compose links)
```
